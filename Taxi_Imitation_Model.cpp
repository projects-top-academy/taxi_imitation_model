﻿
/*

Создать имитационную модель "остановка маршрутных такси". 
Необходимо вводить следующую информацию: среднее время между появлениями пассажиров на остановке в разное время суток, среднее время между появлениями маршруток на остановке в разное время суток, тип остановки (конечная или нет). 
Необходимо определить: среднее время пребывание человека на остановке, достаточный интервал времени между приходами маршруток, чтобы на остановке находилось не более N людей одновременно. Количество свободных мест в маршрутке является случайной величиной.

*/

#include <iostream>
#include <random>

int getRandomNumber(int min, int max) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(min, max);
    return dis(gen);
}

int main()
{
    int avgPassgengerArrivalTimeDay = 5;
    int avgPassgengerArrivalTimeNight = 10;

    int avgVanArrivalTimeDay = 15;
    int avgVanArrivalTimeNight = 20;

    bool isTerminalStop = false;

    int maxPeopleAtStop = 10;

    int avgStayTime = (isTerminalStop) ? 30 : 20;

    int maxVanArrivalInterval = (maxPeopleAtStop * avgStayTime) / 2;

    int freeSeatsInVan = getRandomNumber(5, 15);

    std::cout << "The average time a person stays at a bus stop: " << avgStayTime << " minutes\n";
    std::cout << "Sufficient time interval between arrivals of minibuses: " << maxVanArrivalInterval << " minutes\n";
    std::cout << "The number of available seats in the minibus: " << freeSeatsInVan << "\n";

    return 0;
}
